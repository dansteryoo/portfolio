export const upcomingsData = [
	{
		name: 'NCA EM Website',
		description: 'The website for New Church of Atlanta English Ministry',
		githubUrl: '#',
		techItems: ['React', 'Material UI', 'Firebase', 'React Hook Form', 'Yup'],
		siteUrl: 'https://newchurchatl.com',
	},
	{
		name: 'Registrations.church',
		description: 'A website to help non-profits manage their events.',
		githubUrl: '#',
		techItems: ['React', 'Nextjs', 'Apollo', 'Node.js', 'MongoDb'],
		siteUrl: 'https://registrations.church',
	},
	{
		name: 'Off the Pulpit Podcast',
		description:
			'Three pastors & friends conversing about church, culture, and life off the pulpit.',
		githubUrl: '#',
		techItems: ['React', 'Nextjs', 'Vercel', 'Firebase'],
		siteUrl: 'https://offthepulpitpod.vercel.app',
	},
];
